<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Pagedetails;
use App\User;
use Session;

class PagesController extends Controller
{
    public function index()
    {
        $result =Pagedetails::distinct()->select('pageName')->groupBy('pageName')->get();
        return view('admin.pages.list',compact('result'));
    }

    public function getPageDetail($pageName)
    {
        $result =Pagedetails::Where('pageName',$pageName)->get();

        $introcount =Pagedetails::Where('pageName',$pageName)->where('sectionName', 'intro')->count();
        $herocount =Pagedetails::Where('pageName',$pageName)->where('sectionName', 'hero')->count();
        $statscount =Pagedetails::Where('pageName',$pageName)->where('sectionName', 'stats')->count();
        $slugcount =Pagedetails::Where('pageName',$pageName)->where('sectionName', 'slug')->count();
        $taglinecount =Pagedetails::Where('pageName',$pageName)->where('sectionName', 'tagline')->count();
        $metacount =Pagedetails::Where('pageName',$pageName)->where('sectionName', 'metas')->count();
        // $slugcount =Pagedetails::Where('pageName',$pageName)->where('sectionName', 'metas')->count();
        //dd($resultintro);
        return view('admin.pages.editPageDetail',compact('result','introcount','herocount','statscount','slugcount','taglinecount','metacount'));
    }
    public function getPageSectionDetail($pageName,$sectionName,$subSectionId)
    {
        $result =Pagedetails::Where('pageName',$pageName)->where('sectionName',$sectionName)->where('subSectionId',$subSectionId)->get();
        return view('admin.pages.editPageDetailText',compact('result'));
    }

    public function updatePageSectionDetail($pageName,$sectionName,$subSectionId, Request $request)
    {
        $this->validate($request, [
            'subSectionTitle' => 'sometimes',
            'subSectionText' => 'required',
        ]);
        $input= $request->all();
        unset($input['_token']);

        //dd($input);
        //  dd($input);

        Pagedetails::Where('pageName',$pageName)->where('sectionName',$sectionName)->where('subSectionId',$subSectionId)->update($input);

        activity()->log('Page: '.$pageName.' with '.$sectionName.' section is updated');

        return Redirect('/admin/pagedetail/'.$pageName);


    }


    public function userList()
    {
        $userList = User::all();
        return view('admin.userList', compact('userList'));
    }

    public function updateStatus($userId, $status)
    {
        if($status == 'active'){
            $userStatus = 1;
        }else{
            $userStatus = 0;
        }
        $userDetails = User::find($userId);
        $userDetails->update(array('status'=>$userStatus));
        activity()->log('User '.$userDetails['first_name'] .' '.$userDetails['last_name'] .' status marked '.$status);
        \Session::flash('flash_message','User '.$userDetails['first_name'] .' '.$userDetails['last_name'].' status marked '.$status);
        return redirect()->back();
    }
}
