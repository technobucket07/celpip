<?php

namespace App\Http\Controllers\Admin;

use App\CompanyInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use Spatie\Activitylog\Models\Activity;


class SettingController extends Controller
{
    public function index()
    {
        $id  = CompanyInfo::All()->max('companyId');
        if(isset($id)) {
            $result = CompanyInfo::where('companyId', $id)->get();
            return view('admin.setting', compact('result'));
        }
        else
        {
            return view('admin.setting');
        }
    }

    public function updateSetting(Request $request)
    {
        $this->validate($request, [
            "companyName" => "required",
              "email" => "required",
              "phone" => "required",
              "address" => "required",
        ]);
        $input= $request->all();

        unset($input['_token']);
        unset($input['_wysihtml5_mode']);

        if(isset($input['logoPath']) != false) {
            if ($input['logoPath'] == '') {
                unset($input['logoPath']);
            } else {
                $imageName = 'logoPath' . Carbon::now()->timestamp . '.' . $request->file('logoPath')->getClientOriginalExtension();
                $request->file('logoPath')->move(
                    base_path() . '/public/images/', $imageName);

                $input['logoPath'] = $imageName;
            }
        }

        //dd($input);

        $id  = CompanyInfo::All()->max('companyId');

        if(isset($id)) {
            CompanyInfo::where('companyId',$id)->update($input);
        }
        else
        {
            CompanyInfo::create($input);
        }




        \Session::flash('flash_message','successfully saved.');

        activity()->log('Website Settings are updated');

        //\activity()::

        return redirect()->route('AdminSettingRoute');
    }
}
