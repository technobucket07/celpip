<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pagedetails extends Model
{
     use SoftDeletes;
     protected $table='pagedetails';
}
