<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('test_instruction');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>'auth'],function() {
    Route::get('/admin/dashboard', function () {
        return view('admin.dashboard');
    });

    Route::get('/admin/pagedetail', 'Admin\PagesController@index');
    Route::get('/admin/pagedetail/{pageName}', 'Admin\PagesController@getPageDetail');
    Route::get('/admin/pagedetail/{pageName}/{sectionName}/{subSectionId}', 'Admin\PagesController@getPageSectionDetail');
    Route::post('/admin/pagedetail/{pageName}/{sectionName}/{subSectionId}/update', 'Admin\PagesController@updatePageSectionDetail');

    Route::get('/admin/setting', ['as' => 'AdminSettingRoute', 'uses' => 'Admin\SettingController@index']);
    Route::post('/admin/setting/update', ['as' => 'AdminUpdateSettingRoute', 'uses' => 'Admin\SettingController@updateSetting']);

    Route::get('admin/speaking/create');

});