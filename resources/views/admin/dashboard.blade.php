@extends('admin.shared.adminMaster')


@section('content')

        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--<li class="active">Blank page</li>-->
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">

        <div class="col-md-12">
            <!-- general form elements -->
            <h1 style="text-align: center;margin-top: 120px;font-weight: 200">Welcome to</h1>
            <div class="login-logo">
                <a href="#">
                    <b> {{$companyInfo['companyName']}} </b>
                </a>
            </div>
            <h1 style="text-align: center;font-weight: 200">Control Panel</h1>


            <!-- /.box -->
        </div>

    </div>
</section>
<!-- /.content -->

@endsection