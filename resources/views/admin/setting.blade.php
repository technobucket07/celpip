@extends('admin.shared.adminMaster')


@section('content')
    <link rel="stylesheet" href="{{Url('admin-css/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Setting
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Setting</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    <div class="row">

        @if(Session::has('flash_message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
        @endif

        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Setting</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {{Form::open(['url' => '/admin/setting/update','files'=>'true'])}}
                <div class="box-body">

                    <div class="form-group">
                        <label for="companyName">Company Name</label>
                        @if(isset($result[0]['companyName']))
                        {{Form::text('companyName', $result[0]['companyName'] ,array_merge(['class' => 'form-control']))}}
                            @else
                            {{Form::text('companyName', null ,array_merge(['class' => 'form-control']))}}
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="logoPath">Company Logo</label>
                        {{Form::file('logoPath')}}
                        <br/>
                        @if(isset($result[0]['logoPath']))
                                @if($result[0]['logoPath']=='')
                                    <img src="{{URL('/images/no-image.png')}}" style="width: 150px" />
                                @else
                                    <img src="{{URL('/images/'.$result[0]['logoPath'])}}" style="width: 150px" />
                                @endif
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="email">Company Email</label>
                        @if(isset($result[0]['email']))
                        {{Form::text('email', $result[0]['email'] ,array_merge(['class' => 'form-control']))}}
                            @else
                            {{Form::text('email', null ,array_merge(['class' => 'form-control']))}}
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="phone">Company Phone</label>
                        @if(isset($result[0]['phone']))
                        {{Form::text('phone', $result[0]['phone'] ,array_merge(['class' => 'form-control']))}}
                            @else
                            {{Form::text('phone', null ,array_merge(['class' => 'form-control']))}}
                            @endif
                    </div>

                    <div class="form-group">
                        <label for="address">Company Address</label>
                        @if(isset($result[0]['address']))
                        {{Form::text('address', $result[0]['address'] ,array_merge(['class' => 'form-control']))}}
                                @else
                            {{Form::text('address',null ,array_merge(['class' => 'form-control']))}}
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="address">Company Description</label>
                        @if(isset($result[0]['companyDescription']))
                        {{Form::textarea('companyDescription', $result[0]['companyDescription'] ,array_merge(['class' => 'textarea form-control']))}}
                            @else
                            {{Form::textarea('companyDescription', null ,array_merge(['class' => 'textarea form-control']))}}
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="addresslocality">Address Locality</label>
                        @if(isset($result[0]['addressLocality']))
                             {{Form::text('addressLocality', $result[0]['addressLocality'] ,array_merge(['class' => 'form-control']))}}
                            @else
                            {{Form::text('addressLocality', null ,array_merge(['class' => 'form-control']))}}
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="addresslocality">Address Region</label>
                        @if(isset($result[0]['addressRegion']))
                        {{Form::text('addressRegion', $result[0]['addressRegion'] ,array_merge(['class' => 'form-control']))}}
                            @else
                            {{Form::text('addressRegion',null ,array_merge(['class' => 'form-control']))}}
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="streetAdress">Street Address </label>
                        @if(isset($result[0]['streetAddress']))
                        {{Form::text('streetAddress', $result[0]['streetAddress'] ,array_merge(['class' => 'form-control']))}}
                            @else
                            {{Form::text('streetAddress', null ,array_merge(['class' => 'form-control']))}}
                        @endif
                    </div>

                    @if(isset($result[0]))

                    <div class="form-group">
                        <label for="facebook">Facebook Profile</label>

                        {{Form::text('facebook', $result[0]['facebook'] ,array_merge(['class' => 'form-control']))}}
                    </div>
                    <div class="form-group">
                        <label for="twitter">Twitter Profile</label>
                        {{Form::text('twitter', $result[0]['twitter'] ,array_merge(['class' => 'form-control']))}}
                    </div>
                    <div class="form-group">
                        <label for="google">Google Profile</label>
                        {{Form::text('google', $result[0]['google'] ,array_merge(['class' => 'form-control']))}}
                    </div>
                    <div class="form-group">
                        <label for="linkedin">Linkedin Profile</label>
                        {{Form::text('linkedin', $result[0]['linkedin'] ,array_merge(['class' => 'form-control']))}}
                    </div>
                        @else

                        <div class="form-group">
                            <label for="facebook">Facebook Profile</label>

                            {{Form::text('facebook', null ,array_merge(['class' => 'form-control']))}}
                        </div>
                        <div class="form-group">
                            <label for="twitter">Twitter Profile</label>
                            {{Form::text('twitter', null ,array_merge(['class' => 'form-control']))}}
                        </div>
                        <div class="form-group">
                            <label for="google">Google Profile</label>
                            {{Form::text('google',null ,array_merge(['class' => 'form-control']))}}
                        </div>
                        <div class="form-group">
                            <label for="linkedin">Linkedin Profile</label>
                            {{Form::text('linkedin', null ,array_merge(['class' => 'form-control']))}}
                        </div>


                        @endif


                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    {{Form::button('Save / Update',array_merge(['class' => 'btn btn-primary', 'type'=>'submit']))}}

                </div>
                {{Form::close()}}

            </div>
            <!-- /.box -->
        </div>

    </div>
</section>
<!-- /.content -->
    @include('mceImageUpload::upload_form')
    {{----}}
@endsection

@section('addonjquery')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=cw3o8zabt4oa1k0g76sr3rhccj45itc1jecapcboi24mcu6y"></script>
    <script src="{{Url('admin-css/tinymce-charactercount.plugin.js')}}"></script>
    <script>
        tinymce.init({ selector:'textarea',
            height:300,
            menubar: true,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code',
                    'wordcount',
                    'charactercount'
            ],
            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            relative_urls: false,
            file_browser_callback: function(field_name, url, type, win) {
                // trigger file upload form
                if (type == 'image') $('#formUpload input').click();
            }
        });


        $(":input[type=text]").each(function(){
            var words = $.trim($(this).val()).split(' ');
            var Characters = $(this).val().length;
            if($(this).next('span').length == 0)
            {
                $(this).after('<span></span>');
            }
            $(this).next('span').text('Words: '+words.length+' , Characters: '+Characters);
        });

        $('input[type=text]').keyup(function(){
            var words = $.trim($(this).val()).split(' ');
            var Characters = $(this).val().length;
            if($(this).next('span').length == 0)
            {
                $(this).after('<span></span>');
            }
            $(this).next('span').text('Words: '+words.length+' , Characters: '+Characters);

        });
    </script>
@endsection
