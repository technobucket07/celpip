@extends('admin.shared.adminMaster')


@section('content')

        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Page List
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">pages</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Page List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Page Name</th>
                        <th>Admin</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($result as $item  )

                        <tr>
                            <td> {{$item['pageName']}}</td>
                            <td><div class="btn-group">
                                    <a href="/admin/pagedetail/{{$item['pageName']}}" class="btn btn-warning">Edit</a>
                                    <!--<a href="/admin/pageDetail?pageNamw={{$item['pageName']}}" class="btn btn-danger">Delete</a>-->
                                </div></td>
                        </tr>

                        @endforeach

                    </tbody>

                </table>
            </div>
            <!-- /.box-body -->
        </div>

    </div>
</section>
<!-- /.content -->

@endsection