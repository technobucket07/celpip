@extends('admin.shared.adminMaster')


@section('content')

    <section class="content-header">
        <h1 class="text-capitalize">URL Redirect List</h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">URL Redirect List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <div class="row">
            <div class="box">



                <div class="box-header">
                    <h3 class="box-title">&nbsp;</h3>

                    <div class="col-md-12">
                        {{Form::open(['url' => '/admin/urlredirects/add'])}}
                        <div class="form-group">
                            <label for="content">Source URL</label>
                            {{Form::text('sourceUrl', null ,array_merge(['class' => 'form-control','Placeholder'=>'Add Source Url']))}}
                        </div>
                        <div class="form-group">
                            <label for="content">Destination URL</label>
                            {{Form::text('destinationUrl', null ,array_merge(['class' => 'form-control','Placeholder'=>'Add Destination Url']))}}
                        </div>
                        <div class="col-md-12">
                            {{Form::button('ADD Redirect',array_merge(['class' => 'btn btn-primary', 'type'=>'submit']))}}
                        </div>
                        {{Form::close()}}
                    </div>

                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Source URL </th>
                            <th>Destination URL</th>
                            <th>Admin</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($result as $item)
                            <tr>
                                <td> {{ $item['sourceUrl'] }}</td>
                                <td> {{ $item['destinationUrl'] }}</td>
                                <td><a href="/admin/urlredirects/edit/{{ $item['id'] }}/edit" class="btn btn-warning">Edit</a>
                                    <a href="/admin/urlredirects/delete/{{ $item['id'] }}/delete"  onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                        @endforeach


                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
    </section>
    <!-- /.content -->

@endsection