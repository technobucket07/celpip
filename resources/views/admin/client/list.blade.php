@extends('admin.shared.adminMaster')
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Client
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Client</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if(Session::has('flash_message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"> Client List</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="productList" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Image</th>
                                <th>Text</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($clientList as $client  )
                                    <tr>
                                        <td><img src="{{url('images/client/'.$client['picture'])}}" height="200" width="200"></td>
                                        <td>{!! $client['name'] !!}</td>
                                        <td><a  class="btn btn-warning" href = "{{url('admin/client/details/'.$client['id'])}}" >Edit</a></td>
                                        <td><a  class="btn btn-danger" href = "{{url('admin/client/delete/'.$client['id'])}}"  onclick="return confirm('Are you sure you want to delete this item?');">Delete</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->
@endsection
@section('addonjquery')
<script>
    $(function () {
        $('#productList').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": false,
            "autoWidth": false
        });
    });

</script>
@endsection