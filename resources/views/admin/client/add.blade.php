@extends('admin.shared.adminMaster')
@section('content')
    <section class="content-header">
        <h1>
            Client
        </h1>
        <ol class="breadcrumb">
            <li ><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Client Details</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        @if(Session::has('flash_message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
        @endif
        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="row">

            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Client Detail</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {{Form::model($clientDetails, ['url' => url('admin/client/details/submit'),'files'=>'true'])}}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="content">Name</label>
                            {{Form::text('name',null ,array_merge(['class' => ' form-control']))}}
                        </div>

                        <div class="form-group">
                            @if($picturePath != '')
                                <img src="{{url($picturePath)}}" width="150px"  height="150px"><br>
                            @endif
                            <label for="CourseLogo">Client Image</label>
                            {{Form::file('picture')}}
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        {{Form::hidden('id', $value=null)}}
                        {{Form::hidden('picture', $value=null)}}
                        {{Form::button('Submit Client Details',array_merge(['class' => 'btn btn-primary', 'type'=>'submit']))}}

                    </div>
                    {{Form::close()}}
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>

@endsection

