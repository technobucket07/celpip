<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@if(isset($companyInfo['companyName']))
                {{$companyInfo['companyName']}} |
        @endif</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{Url('admin/bootstrap/css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{Url('css/jquery-ui.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="{{Url('admin/plugins/datatables/dataTables.bootstrap.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{Url('admin/dist/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{Url('admin/dist/css/skins/_all-skins.min.css')}}">
    <link rel="stylesheet" href="{{Url('admin/plugins/timepicker/bootstrap-timepicker.min.css')}}">
    <link rel="stylesheet" href="{{Url('admin/plugins/select2/select2.min.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .mce-charactercount{
            margin-top: 10px !important;
            right: 120px !important;
            position: absolute !important;
        }
        .checkbox{
            height: 20px;
            width: 20px;
        }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="/admin/dashboard" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">CP</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">@if(isset($companyInfo['companyName']))
                    {{$companyInfo['companyName']}}
                @endif</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{Url('admin/dist/img/user2-160x160.jpg')}}" class="user-image"
                                 alt="User Image">
                            <span class="hidden-xs"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{Auth::user()->name}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{Url('admin/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                                <p>
                                    {{Auth::user()->name}}
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                @if(!empty(Auth::user()->name))
                                    <div class="pull-left hidden">
                                        <a href="{{URL('/admin/changepassword')}}" class="btn btn-default btn-flat">Change Password</a>
                                    </div>
                                @endif
                                <div class="pull-right">
                                    @if(!empty(Auth::user()->name))
                                        {{ Form::open(array('url' => 'logout')) }}
                                        <button type="submit" class="btn btn-default btn-flat">Sign out</button>
                                        {{ Form::close() }}
                                    @else
                                        <a href="{{ url('/login') }}" class="btn btn-default btn-flat">Sign In</a>
                                    @endif
                                </div>
                            </li>
                        </ul>
                    </li>


                </ul>
            </div>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{Url('admin/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{Auth::user()->name}}</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- search form -->

            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li><a href="{{Url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                <li><a href="{{Url('admin/pagedetail')}}"><i class="fa fa-files-o"></i> <span>Pages</span></a></li>
                <li class="treeview">
                    <a href="#">
                            <i class="fa fa-globe"></i> <span>Website Settings</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                    </a>
                        <ul class="treeview-menu">
                                <li><a href="{{Url('admin/setting')}}"><i class="fa  fa-cog"></i> <span>General Settings</span></a></li>
                                <li><a href="{{Url('admin/urlredirects')}}"><i class="fa fa-link"></i> <span>URL Redirects</span></a> </li>
                               {{--<li><a href="{{Url('admin/activitylog')}}"><i class="fa  fa-user-secret"></i> <span>Activity Log</span></a> </li>--}}
                        </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-globe"></i> <span>Speaking Test</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{Url('admin/speaking/create')}}"><i class="fa  fa-cog"></i> <span>Add/ Edit</span></a></li>
                        <li><a href="{{Url('admin/speaking/list')}}"><i class="fa fa-link"></i> <span>View</span></a> </li>
                        {{--<li><a href="{{Url('admin/activitylog')}}"><i class="fa  fa-user-secret"></i> <span>Activity Log</span></a> </li>--}}
                    </ul>
                </li>

            </ul>
      </section>
      <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

      @yield('content')


  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
      <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.7
      </div>
      <strong>Copyright &copy; 2017 <a href="#">@if(isset($companyInfo['companyName']))
                  {{$companyInfo['companyName']}}
              @endif</a>.</strong> All rights
      reserved.
  </footer>

</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{Url('admin/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<script src="{{Url('/js/jquery-ui.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{Url('admin/bootstrap/js/bootstrap.min.js')}}"></script>

<script src="{{Url('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{Url('admin/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{Url('admin/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{Url('admin/plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{Url('admin/dist/js/app.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{Url('admin/dist/js/demo.js')}}"></script>

<!-- InputMask -->
<script src="{{Url('admin/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{Url('admin/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{Url('admin/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>

<script src="{{Url('admin/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<script src="{{Url('admin/plugins/select2/select2.min.js')}}"></script>

<script>
  $(function () {
      $("#example1").DataTable();
      $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
      });
  });
    $('.select2').select2();
</script>

@yield('addonjquery')
</body>
</html>
