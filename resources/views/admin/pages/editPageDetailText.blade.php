@extends('admin.shared.adminMaster')


@section('content')
    <link rel="stylesheet" href="{{Url('admin-css/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        @if(\Illuminate\Support\Facades\Request::segment(4) != \Illuminate\Support\Facades\Request::segment(5))
        {{\Illuminate\Support\Facades\Request::segment(3)." ". \Illuminate\Support\Facades\Request::segment(4)." ".\Illuminate\Support\Facades\Request::segment(5)}} Details
        @else
        {{\Illuminate\Support\Facades\Request::segment(3)." ".\Illuminate\Support\Facades\Request::segment(4)}}
        @endif
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--<li class="active">Blank page</li>-->
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">

        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Set {{\Illuminate\Support\Facades\Request::segment(3)}} Page Details</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {{Form::open(['url' => 'admin/pagedetail/'.$result[0]['pageName'].'/'.$result[0]['sectionName'].'/'.$result[0]['subSectionId'].'/update'])}}
                    <div class="box-body">

                        @if($result[0]['sectionName']!="slug" && $result[0]['sectionName']!='tagline' && $result[0]['sectionName']!='metas')


                            <div class="form-group">
                                <label for="title">Title</label>
                                {{ Form::text('subSectionTitle', $result[0]['subSectionTitle'], array_merge(['class' => 'form-control'])) }}

                            </div>

                            <div class="form-group">
                                <label for="content">Content</label>
                                {{Form::textarea('subSectionText', $result[0]['subSectionText'] ,array_merge(['class' => 'textarea form-control']))}}

                            </div>

                            @elseif($result[0]['sectionName']=='tagline' )


                            <div class="form-group">
                                <label for="content">Content</label>
                                {{Form::textarea('subSectionText', $result[0]['subSectionText'] ,array_merge(['class' => 'textarea form-control']))}}

                            </div>


                        @else

                            <div class="form-group">
                                <label for="content">Content</label>
                                {{Form::text('subSectionText', $result[0]['subSectionText'] ,array_merge(['class' => 'form-control']))}}

                            </div>

                         @endif






                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        {{Form::button('Submit',array_merge(['class' => 'btn btn-primary', 'type'=>'submit']))}}

                    </div>
                {{Form::close()}}
            </div>
            <!-- /.box -->
        </div>

    </div>
    <div class="row">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</section>
<!-- /.content -->
    @include('mceImageUpload::upload_form')
@endsection

@section('addonjquery')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=cw3o8zabt4oa1k0g76sr3rhccj45itc1jecapcboi24mcu6y"></script>
    <script src="{{Url('admin-css/tinymce-charactercount.plugin.js')}}"></script>
    <script>
        tinymce.init({ selector:'textarea',
            height:300,
            menubar: true,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code',
                    'wordcount',
                    'charactercount'
            ],
            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            relative_urls: false,
            file_browser_callback: function(field_name, url, type, win) {
                // trigger file upload form
                if (type == 'image') $('#formUpload input').click();
            }
        });

        $(":input[type=text]").each(function(){
            var words = $.trim($(this).val()).split(' ');
            var Characters = $(this).val().length;
            if($(this).next('span').length == 0)
            {
                $(this).after('<span></span>');
            }
            $(this).next('span').text('Words: '+words.length+' , Characters: '+Characters);
        });

        $('input[type=text]').keyup(function(){
            var words = $.trim($(this).val()).split(' ');
            var Characters = $(this).val().length;
            if($(this).next('span').length == 0)
            {
                $(this).after('<span></span>');
            }
            $(this).next('span').text('Words: '+words.length+' , Characters: '+Characters);

        });
    </script>
@endsection
