@extends('admin.shared.adminMaster')


@section('content')

        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--<li class="active">Blank page</li>-->
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">

        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">

                @if($introcount>0)
                <div class="box-header with-border">
                    <h3 class="box-title">Intro Section</h3>
                </div>
                @endif

                <!-- /.box-header -->
                <!-- form start -->
                <div class="row">
                    <!-- /.col -->
                    @foreach($result as $item )

                        @if($item['sectionName']=='intro')
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-yellow"><i class="fa fa-bookmark-o"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">{{$item['subSectionId']}}</span>
                                        <span class="info-box-number">   {{$item['subSectionTitle']}}</span>
                                        <span class="info-box-text"><a href="/admin/pagedetail/{{$item['pageName']}}/{{$item['sectionName']}}/{{$item['subSectionId']}}" class="badge bg-red">edit</a></span>

                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                        @endif
                    <!-- /.col -->
                    @endforeach
                </div>

                    @if($herocount>0)
                <div class="box-header with-border">
                    <h3 class="box-title">Hero Section</h3>
                </div>
                    @endif
                <div class="row">
                    <!-- /.col -->
                    @foreach($result as $item )

                        @if($item['sectionName']=='hero')

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">{{$item['subSectionId']}}</span>
                                        <span class="info-box-number">   {{$item['subSectionTitle']}}</span>
                                        <span class="info-box-text"><a href="/admin/pagedetail/{{$item['pageName']}}/{{$item['sectionName']}}/{{$item['subSectionId']}}" class="badge bg-red">edit</a></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            @endif
                                    <!-- /.col -->
                            @endforeach
                </div>

                    @if($taglinecount>0)
                <div class="box-header with-border">
                    <h3 class="box-title">Tag Line</h3>
                </div>
                    @endif


                <div class="row">
                    <!-- /.col -->
                    @foreach($result as $item )

                        @if($item['sectionName']=='tagline')

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-blue"><i class="fa fa-star"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">{{$item['subSectionId']}}</span>
                                        <span class="info-box-number">   {{$item['subSectionTitle']}}</span>
                                        <span class="info-box-text"><a href="/admin/pagedetail/{{$item['pageName']}}/{{$item['sectionName']}}/{{$item['subSectionId']}}" class="badge bg-red">edit</a></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            @endif



                                    <!-- /.col -->
                            @endforeach
                </div>

                    @if($statscount>0)
                <div class="box-header with-border">
                    <h3 class="box-title">Stats Section</h3>
                </div>
                    @endif

                <div class="row">
                    <!-- /.col -->
                    @foreach($result as $item )

                        @if($item['sectionName']=='stats')
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green"><i class="ion ion-stats-bars"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">{{$item['subSectionId']}}</span>
                                        <span class="info-box-number">   {{$item['subSectionTitle']}}</span>
                                        <span class="info-box-text"><a href="/admin/pagedetail/{{$item['pageName']}}/{{$item['sectionName']}}/{{$item['subSectionId']}}" class="badge bg-red">edit</a></span>

                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            @endif
                                    <!-- /.col -->
                            @endforeach
                </div>

                    @if($metacount>0)
                <div class="box-header with-border">
                    <h3 class="box-title">Meta Tags</h3>
                </div>
                    @endif


                <div class="row">
                    <!-- /.col -->
                    @foreach($result as $item )

                        @if($item['sectionName']=='metas')
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-light-blue-gradient"><i class="fa fa-umbrella"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">{{$item['subSectionId']}}</span>
                                        <span class="info-box-number">   {{$item['subSectionTitle']}}</span>
                                        <span class="info-box-text"><a href="/admin/pagedetail/{{$item['pageName']}}/{{$item['sectionName']}}/{{$item['subSectionId']}}" class="badge bg-red">edit</a></span>

                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            @endif
                                    <!-- /.col -->
                            @endforeach
                </div>

                    @if($slugcount>0)
                <div class="box-header with-border">
                    <h3 class="box-title">Slug Section</h3>
                </div>
                    @endif
                <div class="row">
                    <!-- /.col -->
                    @foreach($result as $item )

                        @if($item['sectionName']=='slug')
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">{{$item['subSectionId']}}</span>
                                        <span class="info-box-number">   {{$item['subSectionTitle']}}</span>
                                        <span class="info-box-text"><a href="/admin/pagedetail/{{$item['pageName']}}/{{$item['sectionName']}}/{{$item['subSectionId']}}" class="badge bg-red">edit</a></span>

                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            @endif
                                    <!-- /.col -->
                            @endforeach
                </div>

            </div>
            <!-- /.box -->
        </div>

    </div>
</section>
<!-- /.content -->

@endsection