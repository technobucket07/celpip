@extends('shared.master')
@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary no-border">
                        <div class="box-header with-border header-grey">
                            <h3 class="box-title"><b>CELPIP Listening</b></h3>
                            <button type="button" class="btn btn-primary pull-right"> Next</button>
                        </div>
                        <div class="box-body inner-data listening-height">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4><b><i class="fa fa-info-circle"></i> Test Instructions</b></h4>
                                    <ul>
                                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection